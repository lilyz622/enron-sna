// Databricks notebook source
// MAGIC %md # Enron Network Analysis
// MAGIC Welcome to the Enron Analysis notebook, broken down into the following sections:
// MAGIC - ETL
// MAGIC - Graph Analysis

// COMMAND ----------

// MAGIC %md ### ETL
// MAGIC where we:
// MAGIC - extract the data from the sql file
// MAGIC - clean the data
// MAGIC - transform the data into rdds and into vertices and edges
// MAGIC - load the data into a graph
// MAGIC 
// MAGIC result:
// MAGIC - eldf: 
// MAGIC   - "employee list" : one element per employee of enron
// MAGIC   - dataframe with fields: 
// MAGIC     - id:integer
// MAGIC     - firstName:string 
// MAGIC     - lastName:string 
// MAGIC     - email:string
// MAGIC - msgdf:
// MAGIC   - "messages" : one element per message
// MAGIC   - dataframe with fields: 
// MAGIC     - mid:integer
// MAGIC     - sender:string
// MAGIC - ridf:
// MAGIC   - "recipient info" : one element per email per recipient
// MAGIC   - dataframe with fields: 
// MAGIC     - mid:integer the id of the message
// MAGIC     - rtype:string the label of the receiver of the email (eg. CC, TO, BCC)
// MAGIC     - remail:string the email address of the receiver
// MAGIC - mdf:
// MAGIC   - "new messages df" that joins ridf and msgdf on mid results in 4 columns : one element per email per recipient WITH SENDER
// MAGIC   - dataframe with fields: 
// MAGIC     - mid:integer the id of the message
// MAGIC     - rtype:string the label of the receiver of the email (eg. CC, TO, BCC)
// MAGIC     - remail:string the email address of the receiver
// MAGIC     - sender:string the email address of the sender
// MAGIC - allEmailAddresses:
// MAGIC   - "all email addresses" : one element per unique email address (of all the employees and their correspondents)
// MAGIC   - dataset with fields:
// MAGIC     - email:string
// MAGIC     - hash:long the hash value of the email
// MAGIC - graph:
// MAGIC   - "social graph" 
// MAGIC   - Edge[srcId: Long, dstId: Long, attr: Int]:
// MAGIC     - srcId: the hashed email address of the sender
// MAGIC     - dstId: the hashed email address of the recipient
// MAGIC     - attr: the weight of the edge (the number of emails sent from sender to recipient)
// MAGIC   - Vertex(VertexId: Long, EmailAddress: EmailAddress):
// MAGIC     - a tuple

// COMMAND ----------

import scala.util.hashing.MurmurHash3
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import scala.collection.mutable.HashMap

// COMMAND ----------

// MAGIC %run ./config

// COMMAND ----------

val raw_file = sc.textFile(raw_file_location)
raw_file.take(5) // demonstrate that file was properly imported

// COMMAND ----------

// parse employeeList and create eldf fraom with 2 columns: int("ID"),char("EMAIL")
// grab all lines that match employeelist insertions
// replace all spaces, replace all single-quotes, split on commas
// create an array

case class Employee(id: Int, firstName: String, lastName:String, email: String, hash: Long)

val el = raw_file.filter(line => line.contains("INSERT INTO employeelist VALUES")).map(line => line.replaceAll("\\s","").split("\\(")(1).split("\\);")(0).split(",")).map(line=>Employee(line(0).toInt,line(1),line(2),line(3),MurmurHash3.stringHash(line(3)).toLong))

// create spark DF from the 2 columnms parsed above.
val eldf = sqlContext.createDataFrame(el)

eldf.describe().show()
eldf.show(5)
eldf.printSchema()

// COMMAND ----------

// # parse "message" and create "msgdf" from with 2 columns: int("MID"),char("SENDER")
// # grab all lines that match message insertions
// # replace all spaces, replace all single-quotes, split on commas
// # create an array:
// #   element 0 is the integer split after parentheses of element 0 above,
// #   element 1 is the email address (element 1 above)

case class Message(mid: Int, sender: String)

val msg = raw_file.filter(line => line.contains("INSERT INTO message VALUES")).map(line => line.replaceAll("\\s","").split("\\(")(1).split("\\);")(0).split(",")).map(line=>Message(line(0).toInt,line(1)))

// create spark DF from the 2 columnms parsed above.
val msgdf = sqlContext.createDataFrame(msg)

msgdf.describe().show()
msgdf.show(5)
msgdf.printSchema()


// COMMAND ----------

// # parse "recipientinfo" and create "ridf" fraom with 2 columns: int("MID"),char("RTYPE"),char("SENDER")
// # grab all lines that match message insertions
// # replace all spaces, replace all single-quotes, split on commas
// # create an array:
// #   element 0 is the integer of element 1 above,
// #   element 1 is the rtype address (element 2 above)
// #   element 2 is the email address (element 3 above)

case class RecipientInfo(mid:Int, rtype:String, remail:String)

val recipInfo = raw_file.filter(line => line.contains("INSERT INTO recipientinfo VALUES")).map(line => line.replaceAll("\\s","").split("\\(")(1).split("\\);")(0).split(",")).map(line=>RecipientInfo(line(1).toInt, line(2), line(3)))

//create spark DF from the 3 columns parsed above
val ridf = sqlContext.createDataFrame(recipInfo)

ridf.describe().show()
ridf.show(5)
ridf.printSchema()

// COMMAND ----------

// join ridf and msgdf on mid results in 4 columns: 
// mid, rtype, remail, sender
val mdf = ridf.join(msgdf, "mid")
mdf.show(25)

// COMMAND ----------

// create dataset of distinct email addresses to be used as vertices

case class EmailAddressHash(email: String, hash:Long)

// create a fast hash/address lookup table
val allEmailAddresses = mdf.flatMap(x=>Iterable(x(2).toString,x(3).toString)).distinct().map(x=>EmailAddressHash(x, MurmurHash3.stringHash(x).toLong))

allEmailAddresses.createOrReplaceTempView("all_email_addresses")

allEmailAddresses.describe().show()
allEmailAddresses.show(5)
allEmailAddresses.printSchema()

// COMMAND ----------

// MAGIC %md ### Creating a graph with all the emails

// COMMAND ----------

// MAGIC %md After running the following code that creates a graph composed of all the emails, I found that we had 75057 vertices, when we only have 151 employees. This means that the number of non-employees is to much to factor into when trying to identify Enron communities. For now, I have left the code in case we want to analyze contact outside of the employee circle later on.
// MAGIC ```
// MAGIC // create vertices and edges formatted to be put in the graph.
// MAGIC case class EmailAddress(email:String)
// MAGIC 
// MAGIC // create vertices of the form (id: Long, object: EmailAddress)
// MAGIC val emailVertices: RDD[(Long, EmailAddress)] = allEmailAddresses.map(x=>(x.hash, EmailAddress(x.email))).rdd
// MAGIC 
// MAGIC // defaultEmail in case edge points to missing vertex
// MAGIC val defaultEmail = (EmailAddress("Missing"))
// MAGIC 
// MAGIC // create edges of the form RDD[Edge(id1, id2, countOfEmails)]
// MAGIC // TODO: make relationships a hashmap with key as relationshipType and value as the count, (eg. key="TO", value=20)
// MAGIC val emailEdges: RDD[Edge[Int]] = mdf
// MAGIC   .map(x=>((MurmurHash3.stringHash(x(2).toString).toLong,MurmurHash3.stringHash(x(3).toString).toLong), 1))
// MAGIC   .rdd
// MAGIC   .reduceByKey((a,b)=> a+b)
// MAGIC   .map(x=> Edge(x._1._1, x._1._2, x._2))
// MAGIC 
// MAGIC // create the graph using vertices and edges
// MAGIC val graph = Graph(emailVertices, emailEdges, defaultEmail)
// MAGIC graph.persist()
// MAGIC 
// MAGIC // make sure graph persisted correctly
// MAGIC val numEdges: Long = graph.numEdges
// MAGIC val numVertices: Long = graph.numVertices
// MAGIC // sum the "weight" of each edge to get a total of 2064442 emails, which is correct
// MAGIC val numEmails = graph.edges.reduce((x,y) => Edge(-1L,0L, (x.attr.toInt+y.attr.toInt)))
// MAGIC 
// MAGIC /*
// MAGIC RESULT: 
// MAGIC numEdges: Long = 322415
// MAGIC numVertices: Long = 75057
// MAGIC numEmails: org.apache.spark.graphx.Edge[Int] = Edge(-1,0,2064442)
// MAGIC */
// MAGIC 
// MAGIC // check for missing vertices - there are none
// MAGIC val numMissing = graph.vertices.filter(x => x._2.email == "Missing").count
// MAGIC ```

// COMMAND ----------

val employeeHashes = eldf.map(x=>(MurmurHash3.stringHash(x.getAs[String]("email")).toLong)).collect()

// COMMAND ----------

// MAGIC %md Legacy
// MAGIC ```
// MAGIC // although there are only 151 employees, we have over 75,000 vertices ... 
// MAGIC // let's cut out the vertices that are non-employee
// MAGIC import scala.collection.immutable.HashSet
// MAGIC 
// MAGIC // 1. broadcast the list of employees
// MAGIC 
// MAGIC val employeeHashes = eldf.map(x=>(MurmurHash3.stringHash(x.getAs[String]("email")).toLong)).rdd.collect()
// MAGIC val parameters = collection.immutable.HashMap({"employees" -> HashSet().++(employeeHashes)})
// MAGIC val bParameters = sc.broadcast(parameters)
// MAGIC 
// MAGIC bParameters.value.get("employees")
// MAGIC 
// MAGIC // 2. create the employee-only graph
// MAGIC 
// MAGIC // create vertices and edges formatted to be put in the graph.
// MAGIC case class EmailAddress(email:String) {override def toString() }
// MAGIC 
// MAGIC // create vertices of the form (id: Long, object: EmailAddress)
// MAGIC val emailVertices: RDD[(Long, EmailAddress)] = eldf
// MAGIC   .map(x=>(MurmurHash3.stringHash(x.getAs[String]("email")).toLong, EmailAddress(x.getAs[String]("email")))).rdd
// MAGIC 
// MAGIC // defaultEmail in case edge points to missing vertex
// MAGIC val defaultEmail = (EmailAddress("Missing"))
// MAGIC 
// MAGIC //create edges of the form RDD[Edge(id1, id2, countOfEmails)]
// MAGIC // TODO: make relationships a hashmap with key as relationshipType and value as the count, (eg. key="TO", value=20)
// MAGIC val emailEdgesRdd = mdf
// MAGIC   .map(x=>((MurmurHash3.stringHash(x(2).toString).toLong,MurmurHash3.stringHash(x(3).toString).toLong), 1L))
// MAGIC   .rdd
// MAGIC   .filter(x => bParameters.value.get("employees").get(x._1._1) && bParameters.value.get("employees").get(x._1._2))
// MAGIC   .reduceByKey((a,b)=> a+b)
// MAGIC 
// MAGIC val emailEdges = emailEdgesRdd.map(x=> Edge(x._1._1, x._1._2, x._2))
// MAGIC 
// MAGIC // 3. create the graph
// MAGIC val graph = Graph(emailVertices, emailEdges, defaultEmail)
// MAGIC graph.removeSelfEdges()
// MAGIC graph.persist()
// MAGIC 
// MAGIC 
// MAGIC // 4. make sure graph persisted correctly
// MAGIC 
// MAGIC // val numEdges: Long = graph.numEdges // 2235
// MAGIC // val numVertices: Long = graph.numVertices //151
// MAGIC 
// MAGIC // // check for missing vertices - there are none
// MAGIC // val numMissing = graph.vertices.filter(x => x._2.email == "Missing").count // 0
// MAGIC 
// MAGIC // // sum the "weight" of each edge to get the total number of inter-employee emails
// MAGIC // val numEmails = graph.edges.reduce((x,y) => Edge(-1L,0L, (x.attr.toInt+y.attr.toInt))) // 50574
// MAGIC ```

// COMMAND ----------

// although there are only 151 employees, we have over 75,000 vertices ... 
// let's cut out the vertices that are non-employee
import scala.collection.immutable.HashSet

// 1. broadcast the list of employees

val employeeHashes = eldf.map(x=>(MurmurHash3.stringHash(x.getAs[String]("email")).toLong)).rdd.collect()
val parameters = collection.immutable.HashMap({"employees" -> HashSet().++(employeeHashes)})
val bParameters = sc.broadcast(parameters)

bParameters.value.get("employees")

// 2. create the employee-only graph

// create vertices of the form (id: Long, object: EmailAddress)
val emailVertices: RDD[(Long, String)] = eldf
  .map(x=>(MurmurHash3.stringHash(x.getAs[String]("email")).toLong, x.getAs[String]("email"))).rdd

// defaultEmail in case edge points to missing vertex
val defaultEmail = ("Missing")

//create edges of the form RDD[Edge(id1, id2, countOfEmails)]
// TODO: make relationships a hashmap with key as relationshipType and value as the count, (eg. key="TO", value=20)
val emailEdgesRdd = mdf
  .map(x=>((MurmurHash3.stringHash(x(2).toString).toLong,MurmurHash3.stringHash(x(3).toString).toLong), 1L))
  .rdd
  .filter(x => bParameters.value.get("employees").get(x._1._1) && bParameters.value.get("employees").get(x._1._2))
  .reduceByKey((a,b)=> a+b)

val emailEdges = emailEdgesRdd.map(x=> Edge(x._1._1, x._1._2, x._2))

// 3. create the graph
val graph = Graph(emailVertices, emailEdges, defaultEmail)
graph.removeSelfEdges()
graph.persist()

// 4. Un-persist unused dataframes
eldf.unpersist()
msgdf.unpersist()
mdf.unpersist()
ridf.unpersist()

// 5. make sure graph persisted correctly

val numEdges: Long = graph.numEdges // 2235
val numVertices: Long = graph.numVertices //151

// // check for missing vertices - there are none
val numMissing = graph.vertices.filter(x => x._2 == "Missing").count // 0

// // sum the "weight" of each edge to get the total number of inter-employee emails
val numEmails = graph.edges.reduce((x,y) => Edge(-1L,0L, (x.attr.toInt+y.attr.toInt))) // 50574

// COMMAND ----------

/* 
output to csv. To download the file:
1. go to data
2. click "add data"
3. click "DBFS"
4. navigate to Filestore/tables
5. copy the link eg. FileStore/tables/nodes.csv/part-00000-tid-2595115853338567611-a2cda32e-cfb5-4d04-ae1f-93be32850a58-4257-c000.csv
6. in your browser, go to https://community.cloud.databricks.com/files/<your link>
eg. https://community.cloud.databricks.com/files/tables/nodes.csv/part-00000-tid-2595115853338567611-a2cda32e-cfb5-4d04-ae1f-93be32850a58-4257-c000.csv
*/
// eldf.coalesce(1).write.csv("/FileStore/tables/eNodes")
// sqlContext.createDataFrame(emailEdges.map(x=>(x.srcId,x.dstId, x.attr))).coalesce(1).write.csv("/FileStore/tables/eEdges")

// COMMAND ----------

// MAGIC %md ### Graph Analysis
// MAGIC where we:
// MAGIC - apply the ... algorithm to determine ...

// COMMAND ----------

// USEFUL FUNCTIONS
import spark.implicits._


case class Vertex(id: Long, value: Int)
def verticeNames(vertices:VertexRDD[Int], graph:Graph[String, Long]): org.apache.spark.sql.DataFrame = {
  val verticesdf = sqlContext.createDataFrame(vertices)
  verticesdf.createOrReplaceTempView("vertices_temp")
  val allVertices = sqlContext.createDataFrame(graph.vertices.map(x=>(x._1, x._2)))
  allVertices.createOrReplaceTempView("all_temp")
  val result = sqlContext.sql(
    "SELECT a._2 email, v._1 AS id, v._2 AS value " +
    "FROM vertices_temp v LEFT JOIN all_temp a ON v._1 = a._1 " +
    "ORDER BY 3 DESC"
  )
  return result
}

def verticeNamesPageRank(vertices:VertexRDD[Double], graph:Graph[String, Long]): org.apache.spark.sql.DataFrame = {
  val verticesdf = sqlContext.createDataFrame(vertices)
  verticesdf.createOrReplaceTempView("vertices_temp")
  val allVertices = sqlContext.createDataFrame(graph.vertices.map(x=>(x._1, x._2)))
  allVertices.createOrReplaceTempView("all_temp")
  val result = sqlContext.sql(
    "SELECT a._2 email, v._1 AS id, v._2 AS value " +
    "FROM vertices_temp v LEFT JOIN all_temp a ON v._1 = a._1 " +
    "ORDER BY 3 DESC"
  )
  return result
}

def graphToCsv( graph:Graph[String,Long], fileName: String): Boolean = {
  val vertices = sqlContext.createDataFrame(
    graph.vertices
    .map(x=>(x._1, x._2))
  ).coalesce(1).write.csv("FileStore/"+fileName+"_eNodes") 
  val edgesCsv = sqlContext.createDataFrame(graph.edges.map(x=>(x.srcId,x.dstId, x.attr))).coalesce(1).write.csv("FileStore/"+fileName+"_eEdges")
  return true;
}

def louvainToCsv( graph:Graph[LouvainData,Long], fileName: String): Boolean = {
  val vertices = sqlContext.createDataFrame(
    graph.vertices
    .map(x=>(x._1, x._2))
  ).coalesce(1).write.csv("FileStore/"+fileName+"_Vertices") 
  val edgesCsv = sqlContext.createDataFrame(graph.edges.map(x=>(x.srcId,x.dstId, x.attr))).coalesce(1).write.csv("FileStore/"+fileName+"_Edges")
  return true;
}

// val allVertices = verticeNames(graph.inDegrees, graph)
// allVertices.show(10)

// COMMAND ----------

// MAGIC %md
// MAGIC // <b>WIP: Trying to import stats library</b>
// MAGIC ```
// MAGIC /*
// MAGIC IMPORTING VEGAS:
// MAGIC 1. Go to Workspace, click Create, click Library
// MAGIC 2. In source, select maven central
// MAGIC 3. Use coordinate org.vegas-viz:vegas_2.11:0.3.11
// MAGIC */
// MAGIC import javafx._
// MAGIC import javafx.embed.swing.JFXPanel
// MAGIC import vegas._
// MAGIC import vegas.render.WindowRenderer._
// MAGIC 
// MAGIC val plot = vegas.Vegas("Country Pop").
// MAGIC   withData(
// MAGIC     Seq(
// MAGIC       Map("country" -> "USA", "population" -> 314),
// MAGIC       Map("country" -> "UK", "population" -> 64),
// MAGIC       Map("country" -> "DK", "population" -> 80)
// MAGIC     )
// MAGIC   ).
// MAGIC   encodeX("country", Nom).
// MAGIC   encodeY("population", Quant).
// MAGIC   mark(Bar)
// MAGIC 
// MAGIC plot.show
// MAGIC ```

// COMMAND ----------

// MAGIC %md <b>Initial Analysis / Basic Stats</b>
// MAGIC 1. Received the most emails: stats and distribution
// MAGIC 2. Sent the most emails: stats and distribution
// MAGIC 3. Received or sent the most emails: stats and distribution

// COMMAND ----------

// Received the most emails
val inDegreesDf = verticeNames(graph.inDegrees, graph)
inDegreesDf.createOrReplaceTempView("indegrees")

// Sent the most emails
val outDegreesDf = verticeNames(graph.outDegrees, graph)
outDegreesDf.createOrReplaceTempView("outdegrees")

// Sent or received the most emails

val allVertices = sqlContext.createDataFrame(graph.vertices.map(x=>(x._1, x._2)))
  allVertices.createOrReplaceTempView("all")

val degreesDf = sqlContext.sql(
  "SELECT a._2 AS email, o.id AS id, (o.value+IF(i.value IS NOT NULL,i.value,0)) AS total_count, o.value AS out_count, i.value AS in_count "+
  "FROM all a JOIN outdegrees o ON a._1 = o.id LEFT JOIN indegrees i ON o.id = i.id "+
  "ORDER BY 3 DESC"
)

degreesDf.createOrReplaceTempView("weighted_degrees")

display(degreesDf)

// COMMAND ----------

// MAGIC %md <b>Triangle Count</b>

// COMMAND ----------

val triCountsVertices = graph.triangleCount().vertices
val triCounts = verticeNames(triCountsVertices, graph)
display(triCounts)

// COMMAND ----------

// MAGIC %md <b>Page Rank</b>

// COMMAND ----------

// Run PageRank
val ranksVertices = graph.pageRank(0.0001).vertices
val ranks = verticeNamesPageRank(ranksVertices, graph)
display(ranks)

// COMMAND ----------

// MAGIC %md <b>Louvain Algorithm</b><br>

// COMMAND ----------

// MAGIC %md note: the below three blocks of code, named <b>DGA LouvainData Class</b>, <b>DGA</b>, <b>DGA Runner</b> are modified from [this repository](https://github.com/Sotera/distributed-graph-analytics/tree/master/dga-graphx) by Sotera under Apache 2.0 License. Because they have not yet published their library, I took it and modified it from their github repository. 

// COMMAND ----------

/**
 * Louvain vertex state
 * Contains all information needed for louvain community detection
 */
class LouvainData(var name: String, var community: Long, var communityName: String, var communitySigmaTot: Long, var internalWeight: Long, var nodeWeight: Long, var changed: Boolean) extends Serializable  {

  def this() = this(null, -1L, null, 0L, 0L, 0L, false)


  override def toString: String = s"{name:$name,community:$community,communityName:$communityName,communitySigmaTot:$communitySigmaTot,internalWeight:$internalWeight,nodeWeight:$nodeWeight}"

}



// COMMAND ----------

import org.apache.spark.SparkContext._
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.graphx._

import scala.reflect.ClassTag

/**
 * Provides low level louvain community detection algorithm functions.  Generally used by LouvainHarness
 * to coordinate the correct execution of the algorithm through its several stages.
 *
 * For details on the sequential algorithm see:  Fast unfolding of communities in large networks, Blondel 2008
 */
class LouvainCore extends Serializable {

  /**
   * Generates a new graph of type Graph[VertexState,Long] based on an input graph of type.
   * Graph[VD,Long].  The resulting graph can be used for louvain computation.
   *
   */
  def createLouvainGraph[VD: ClassTag](graph: Graph[VD, Long]): Graph[LouvainData, Long] = {
    // Create the initial Louvain graph.  
    //val nodeWeightMapFunc = (e: EdgeTriplet[VD, Long]) => Iterator((e.srcId, e.attr), (e.dstId, e.attr))
    //val nodeWeightReduceFunc = (e1: Long, e2: Long) => e1 + e2
    //val nodeWeights = graph.mapReduceTriplets(nodeWeightMapFunc, nodeWeightReduceFunc)

    val nodeWeights = graph.aggregateMessages(
      (e:EdgeContext[VD,Long,Long]) => {
        e.sendToSrc(e.attr)
        e.sendToDst(e.attr)
      },
      (e1: Long, e2: Long) => e1 + e2
    )

    graph.outerJoinVertices(nodeWeights)((vid, data, weightOption) => {
      val weight = weightOption.getOrElse(0L)
      val name = data.asInstanceOf[String]
      new LouvainData(name, vid, name, weight, 0L, weight, false)
    }).partitionBy(PartitionStrategy.EdgePartition2D).groupEdges(_ + _)
  }

  /**
   * Transform a graph from [VD,Long] to a a [VertexState,Long] graph and label each vertex with a community
   * to maximize global modularity (without compressing the graph)
   */
  def louvainFromStandardGraph[VD: ClassTag](sc: SparkContext, graph: Graph[VD, Long], minProgress: Int = 1, progressCounter: Int = 1): (Double, Graph[LouvainData, Long], Int) = {
    val louvainGraph = createLouvainGraph(graph)
    louvain(sc, louvainGraph, minProgress, progressCounter)
  }

  /**
   * For a graph of type Graph[VertexState,Long] label each vertex with a community to maximize global modularity. 
   * (without compressing the graph)
   */
  def louvain(sc: SparkContext, graph: Graph[LouvainData, Long], minProgress: Int = 1, progressCounter: Int = 1): (Double, Graph[LouvainData, Long], Int) = {
    var louvainGraph = graph.cache()
    val graphWeight = louvainGraph.vertices.map(louvainVertex => {
      val (vertexId, louvainData) = louvainVertex
      louvainData.internalWeight + louvainData.nodeWeight
    }).reduce(_ + _)
    val totalGraphWeight = sc.broadcast(graphWeight)
    println("totalEdgeWeight: " + totalGraphWeight.value)

    // gather community information from each vertex's local neighborhood
    // var communityRDD = louvainGraph.mapReduceTriplets(sendCommunityData, mergeCommunityMessages)
    var communityRDD = louvainGraph.aggregateMessages(sendCommunityData,mergeCommunityMessages)

    var activeMessages = communityRDD.count() //materializes the msgRDD and caches it in memory

    var updated = 0L - minProgress
    var even = false
    var count = 0
//     val maxIter = 100000
    val maxIter = 1000
    var stop = 0
    var updatedLastPhase = 0L
    do {
      count += 1
      even = !even

      // label each vertex with its best community based on neighboring community information
      val labeledVertices = louvainVertJoin(louvainGraph, communityRDD, totalGraphWeight, even).cache()
      println("***labeledVertices:")
      labeledVertices.collect().foreach(println)

      // calculate new sigma total value for each community (total weight of each community)
      val communityUpdate = labeledVertices
        .map({ case (vid, vdata) => (vdata.community, vdata.nodeWeight + vdata.internalWeight)})
        .reduceByKey(_ + _).cache()

      // map each vertex ID to its updated community information
      val communityMapping = labeledVertices
        .map({ case (vid, vdata) => (vdata.community, vid)})
        .join(communityUpdate)
        .map({ case (community, (vid, sigmaTot)) => (vid, (community, sigmaTot))})
        .cache()

      // join the community labeled vertices with the updated community info
      val updatedVertices = labeledVertices.join(communityMapping).map({ case (vertexId, (louvainData, communityTuple)) =>
        val (community, communitySigmaTot) = communityTuple
        louvainData.community = community
        louvainData.communitySigmaTot = communitySigmaTot
        (vertexId, louvainData)
      }).cache()

      labeledVertices.unpersist(blocking = false)
      communityUpdate.unpersist(blocking = false)
      communityMapping.unpersist(blocking = false)

      val prevG = louvainGraph
      louvainGraph = louvainGraph.outerJoinVertices(updatedVertices)((vid, old, newOpt) => newOpt.getOrElse(old))
      louvainGraph.cache()

      // gather community information from each vertex's local neighborhood
      val oldMsgs = communityRDD
      //communityRDD = louvainGraph.mapReduceTriplets(sendCommunityData, mergeCommunityMessages).cache()
      communityRDD = louvainGraph.aggregateMessages(sendCommunityData, mergeCommunityMessages).cache()
      activeMessages = communityRDD.count() // materializes the graph by forcing computation

      oldMsgs.unpersist(blocking = false)
      updatedVertices.unpersist(blocking = false)
      prevG.unpersistVertices(blocking = false)

      // half of the communities can switch on even cycles
      // and the other half on odd cycles (to prevent deadlocks)
      // so we only want to look for progress on odd cycles (after all vertices have had a chance to move)
      if (even) updated = 0
      updated = updated + louvainGraph.vertices.filter(_._2.changed).count
      
      if (!even) {
        println("  # vertices moved: " + java.text.NumberFormat.getInstance().format(updated))
        if (updated >= updatedLastPhase - minProgress) stop += 1
        updatedLastPhase = updated
      }
    } while (stop <= progressCounter && (even || (updated > 0 && count < maxIter)))
    println("\nCompleted in " + count + " cycles")

    // Use each vertex's neighboring community data to calculate the global modularity of the graph
    val newVertices = louvainGraph.vertices.innerJoin(communityRDD)((vertexId, louvainData, communityMap) => {
      // sum the nodes internal weight and all of its edges that are in its community
      val community = louvainData.community
      var accumulatedInternalWeight = louvainData.internalWeight
      val sigmaTot = louvainData.communitySigmaTot.toDouble
      def accumulateTotalWeight(totalWeight: Long, item: ((Long, (String, Long)), Long)) = {
        val ((communityId, sigmaTotal), communityEdgeWeight) = item
        if (louvainData.community == communityId)
          totalWeight + communityEdgeWeight
        else
          totalWeight
      }
      accumulatedInternalWeight = communityMap.foldLeft(accumulatedInternalWeight)(accumulateTotalWeight)
      val M = totalGraphWeight.value
      val k_i = louvainData.nodeWeight + louvainData.internalWeight
      val q = (accumulatedInternalWeight.toDouble / M) - ((sigmaTot * k_i) / math.pow(M, 2))
      //println(s"vid: $vid community: $community $q = ($k_i_in / $M) -  ( ($sigmaTot * $k_i) / math.pow($M, 2) )")
      if (q < 0)
        0
      else
        q
    })
    val actualQ = newVertices.values.reduce(_ + _)

    // return the modularity value of the graph along with the 
    // graph. vertices are labeled with their community
    (actualQ, louvainGraph, count / 2)
  }

  /**
   * Creates the messages passed between each vertex to convey neighborhood community data.
   */
  private def sendCommunityData(e: EdgeContext[LouvainData, Long, Map[(Long, (String, Long)), Long]]) = {
    val m1 = (Map((e.srcAttr.community, (e.srcAttr.communityName, e.srcAttr.communitySigmaTot)) -> e.attr))
    val m2 = (Map((e.dstAttr.community, (e.dstAttr.communityName, e.dstAttr.communitySigmaTot)) -> e.attr))
    e.sendToSrc(m2)
    e.sendToDst(m1)
  }

  /**
   * Merge neighborhood community data into a single message for each vertex
   */
  private def mergeCommunityMessages(m1: Map[(Long, (String, Long)), Long], m2: Map[(Long, (String, Long)), Long]) = {
    val newMap = scala.collection.mutable.HashMap[(Long, (String, Long)), Long]()
    m1.foreach({ case (k, v) =>
      if (newMap.contains(k)) newMap(k) = newMap(k) + v
      else newMap(k) = v
    })
    m2.foreach({ case (k, v) =>
      if (newMap.contains(k)) newMap(k) = newMap(k) + v
      else newMap(k) = v
    })
    newMap.toMap
  }

  /**
   * Join vertices with community data from their neighborhood and select the best community for each vertex to maximize change in modularity.
   * Returns a new set of vertices with the updated vertex state.
   */
  private def louvainVertJoin(louvainGraph: Graph[LouvainData, Long], msgRDD: VertexRDD[Map[(Long, (String, Long)), Long]], totalEdgeWeight: Broadcast[Long], even: Boolean) = {
    louvainGraph.vertices.innerJoin(msgRDD)((vid, louvainData, communityMessages) => {
      var bestCommunity = louvainData.community
      var bestCommunityName = louvainData.communityName
      val startingCommunityId = bestCommunity
      var maxDeltaQ = BigDecimal(0.0);
      var bestSigmaTot = 0L
      communityMessages.foreach({ case ((communityId, (communityName, sigmaTotal)), communityEdgeWeight) =>
        val deltaQ = q(startingCommunityId, communityId, sigmaTotal, communityEdgeWeight, louvainData.nodeWeight, louvainData.internalWeight, totalEdgeWeight.value)
        //println("   communtiy: "+communityId+" sigma:"+sigmaTotal+" edgeweight:"+communityEdgeWeight+"  q:"+deltaQ)
        if (deltaQ > maxDeltaQ || (deltaQ > 0 && (deltaQ == maxDeltaQ && communityId > bestCommunity))) {
          maxDeltaQ = deltaQ
          bestCommunity = communityId
          bestCommunityName = communityName
          bestSigmaTot = sigmaTotal
        }
      })
      // only allow changes from low to high communities on even cycles and high to low on odd cycles
      if (louvainData.community != bestCommunity && ((even && louvainData.community > bestCommunity) || (!even && louvainData.community < bestCommunity))) {
        //println("  "+vid+" SWITCHED from "+vdata.community+" to "+bestCommunity)
        louvainData.community = bestCommunity
        louvainData.communityName = bestCommunityName
        louvainData.communitySigmaTot = bestSigmaTot
        louvainData.changed = true
      }
      else {
        louvainData.changed = false
      }
      if (louvainData == null)
        println("vdata is null: " + vid)
      louvainData
    })
  }

  /**
   * Returns the change in modularity that would result from a vertex moving to a specified community.
   */
  private def q(currCommunityId: Long, testCommunityId: Long, testSigmaTot: Long, edgeWeightInCommunity: Long, nodeWeight: Long, internalWeight: Long, totalEdgeWeight: Long): BigDecimal = {
    val isCurrentCommunity = currCommunityId.equals(testCommunityId)
    val M = BigDecimal(totalEdgeWeight)
    val k_i_in_L = if (isCurrentCommunity) edgeWeightInCommunity + internalWeight else edgeWeightInCommunity
    val k_i_in = BigDecimal(k_i_in_L)
    val k_i = BigDecimal(nodeWeight + internalWeight)
    val sigma_tot = if (isCurrentCommunity) BigDecimal(testSigmaTot) - k_i else BigDecimal(testSigmaTot)

    var deltaQ = BigDecimal(0.0)
    if (!(isCurrentCommunity && sigma_tot.equals(BigDecimal.valueOf(0.0)))) {
      deltaQ = k_i_in - (k_i * sigma_tot / M)
      //println(s"      $deltaQ = $k_i_in - ( $k_i * $sigma_tot / $M")
    }
    deltaQ
  }

  /**
   * Compress a graph by its communities, aggregate both internal node weights and edge
   * weights within communities.
   */
  def compressGraph(graph: Graph[LouvainData, Long], debug: Boolean = true): Graph[LouvainData, Long] = {

    // aggregate the edge weights of self loops. edges with both src and dst in the same community.
    // WARNING  can not use graph.mapReduceTriplets because we are mapping to new vertexIds
    val internalEdgeWeights = graph.triplets.flatMap(et => {
      if (et.srcAttr.community == et.dstAttr.community) {
        Iterator(((et.srcAttr.community, et.srcAttr.communityName), 2 * et.attr)) // count the weight from both nodes
      }
      else Iterator.empty
    }).reduceByKey(_ + _)

    // aggregate the internal weights of all nodes in each community
    val internalWeights = graph.vertices.values.map(vdata => ((vdata.community, vdata.communityName), vdata.internalWeight)).reduceByKey(_ + _)

    // join internal weights and self edges to find new internal weight of each community
    val newVertices = internalWeights.leftOuterJoin(internalEdgeWeights).map({ case ((vid, communityName), (weight1, weight2Option)) =>
      val weight2 = weight2Option.getOrElse(0L)
      val state = new LouvainData()
      state.name = communityName
      state.community = vid
      state.communityName = communityName
      state.changed = false
      state.communitySigmaTot = 0L
      state.internalWeight = weight1 + weight2
      state.nodeWeight = 0L
      (vid, state)
    }).cache()

    // translate each vertex edge to a community edge
    val edges = graph.triplets.flatMap(et => {
      val src = math.min(et.srcAttr.community, et.dstAttr.community)
      val dst = math.max(et.srcAttr.community, et.dstAttr.community)
      if (src != dst) Iterator(new Edge(src, dst, et.attr))
      else Iterator.empty
    }).cache()

    // generate a new graph where each community of the previous
    // graph is now represented as a single vertex
    val compressedGraph = Graph(newVertices, edges)
      .partitionBy(PartitionStrategy.EdgePartition2D).groupEdges(_ + _)

    // calculate the weighted degree of each node
    //val nodeWeightMapFunc = (e: EdgeTriplet[LouvainData, Long]) => Iterator((e.srcId, e.attr), (e.dstId, e.attr))
    //val nodeWeightReduceFunc = (e1: Long, e2: Long) => e1 + e2
    //val nodeWeights = compressedGraph.mapReduceTriplets(nodeWeightMapFunc, nodeWeightReduceFunc)

    val nodeWeights = compressedGraph.aggregateMessages(
      (e:EdgeContext[LouvainData,Long,Long]) => {
        e.sendToSrc(e.attr)
        e.sendToDst(e.attr)
      },
      (e1: Long, e2: Long) => e1 + e2
    )

    // fill in the weighted degree of each node
    // val louvainGraph = compressedGraph.joinVertices(nodeWeights)((vid,data,weight)=> {
    val louvainGraph = compressedGraph.outerJoinVertices(nodeWeights)((vid, data, weightOption) => {
      val weight = weightOption.getOrElse(0L)
      data.communitySigmaTot = weight + data.internalWeight
      data.nodeWeight = weight
      data
    }).cache()
    louvainGraph.vertices.count()
    louvainGraph.triplets.count() // materialize the graph

    newVertices.unpersist(blocking = false)
    edges.unpersist(blocking = false)

    louvainGraph
  }

  // debug printing
  private def printlouvain(graph: Graph[LouvainData, Long]) = {
    print("\ncommunity label snapshot\n(vid,community,sigmaTot)\n")
    graph.vertices.mapValues((vid, vdata) => (vdata.community, vdata.communitySigmaTot)).collect().foreach(f => println(" " + f))
  }

  // debug printing
  private def printedgetriplets(graph: Graph[LouvainData, Long]) = {
    print("\ncommunity label snapshot FROM TRIPLETS\n(vid,community,sigmaTot)\n")
    graph.triplets.flatMap(e => Iterator((e.srcId, e.srcAttr.community, e.srcAttr.communitySigmaTot), (e.dstId, e.dstAttr.community, e.dstAttr.communitySigmaTot))).collect().foreach(f => println(" " + f))
  }
}

// COMMAND ----------

// DGA RUNNER CONFIGURABLE VARIABLES
var minimumCompressionProgress: Int = 1000 // default 2000
var progressCounter: Int = 1 // default 1
protected var qValues: Array[(Int, Double)] = null

def run[VD:ClassTag](sc: SparkContext, graph: Graph[VD, Long]): Graph[LouvainData,Long] = {
 
    val louvainCore = new LouvainCore
    var louvainGraph = louvainCore.createLouvainGraph(graph)

    var compressionLevel = -1 // number of times the graph has been compressed
    var q_modularityValue = -1.0 // current modularity value
    var halt = false
    do {
      compressionLevel += 1
      println(s"\nStarting Louvain level $compressionLevel")

      // label each vertex with its best community choice at this level of compression
      val (currentQModularityValue, currentGraph, numberOfPasses) = louvainCore.louvain(sc, louvainGraph, minimumCompressionProgress, progressCounter)
      louvainGraph.unpersistVertices(blocking = false)
      louvainGraph = currentGraph
      println(louvainGraph.numVertices)
      println(louvainGraph.numEdges)
      
      louvainToCsv(louvainGraph, "LouvainLevel"+compressionLevel)
      
      // If modularity was increased by at least 0.001 compress the graph and repeat
      // halt immediately if the community labeling took less than 3 passes
      //println(s"if ($passes > 2 && $currentQ > $q + 0.001 )")
//       if (numberOfPasses > 2 && currentQModularityValue > q_modularityValue + 0.001) {
      if (currentQModularityValue > q_modularityValue + 0.001) {
//         saveLevel(sc, compressionLevel, currentQModularityValue, louvainGraph)
        q_modularityValue = currentQModularityValue
        louvainGraph = louvainCore.compressGraph(louvainGraph)
      }
      else {
        halt = true
      }
      
//       if(compressionLevel == 0){
//         halt = true
//       }

    } while (!halt)
//     finalSave(sc, compressionLevel, q_modularityValue, louvainGraph)
  return louvainGraph
}

// COMMAND ----------

sqlContext.sql("set spark.executor.memory = 8g"); 
val louvainGraph = run(sc,graph)

// COMMAND ----------

louvainGraph.triplets.collect().foreach(println)

// COMMAND ----------

louvainGraph.numVertices
louvainGraph.numEdges

// COMMAND ----------

// MAGIC %md <b>Analyze our biggest community</b>

// COMMAND ----------

val biggestCommunity: List[Long] = List(852535950,
2064054424,
1111561425,
-1935529904,
1061363396,
-1683364162,
735519678,
-1889840000,
-2048733777,
-1195574542,
1573644570,
100025426,
-1256263966,
-1752677801,
-261762731,
-1992903630,
-54407617,
-1272962815,
-57257899)

// COMMAND ----------

val parameters = collection.immutable.HashMap({"biggest" -> HashSet().++(biggestCommunity)})
val bParameters = sc.broadcast(parameters)

bParameters.value.get("biggest")

val biggestCommunityGraph = graph.subgraph(vpred = (id, attr) => bParameters.value.get("biggest").get(id))

// COMMAND ----------

val ranksVertices = biggestCommunityGraph.pageRank(0.0001).vertices
val ranks = verticeNamesPageRank(ranksVertices, graph)
display(ranks)

// COMMAND ----------

val triCountsVertices = biggestCommunityGraph.triangleCount().vertices
val triCounts = verticeNames(triCountsVertices, graph)
display(triCounts)
